#include <stdbool.h>
#include <string.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>
#include <esp_log.h>
#include "led_fx_effects.h"
#include "led_color.h"

#define TIMER_DIVIDER         (16)  //  Hardware timer clock divider
#define TIMER_SCALE           (TIMER_BASE_CLK / TIMER_DIVIDER / 100)  // convert counter value to seconds

static const char *TAG = "led_fx";
#define STRIP_FX_CHECK(a, str, goto_tag, ret_value, ...)                      \
    if (!(a))                                                                 \
    {                                                                         \
        ESP_LOGE(TAG, "%s(%d): " str, __FUNCTION__, __LINE__, ##__VA_ARGS__); \
        ret = ret_value;                                                      \
        goto goto_tag;                                                        \
    }

typedef struct {
    int timer_group;
    int timer_idx;
    int alarm_interval;
} led_effect_timer_info_t;

typedef struct {
    led_effect_timer_info_t info;
    uint64_t timer_counter_value;
} led_effect_timer_event_t;

static led_fx_mode led_fx_modes[] = {
    &led_fx_mode_static,
    &led_fx_mode_blink,
    &led_fx_mode_blink_rainbow,
    &led_fx_mode_strobe,
    &led_fx_mode_strobe_rainbow,
    &led_fx_mode_color_wipe,
    &led_fx_mode_color_wipe_inv,
    &led_fx_mode_color_wipe_rev,
    &led_fx_mode_color_wipe_rev_inv,
    &led_fx_mode_color_wipe_random,
    &led_fx_mode_color_sweep_random,
    &led_fx_mode_random_color,
    &led_fx_mode_single_dynamic,
    &led_fx_mode_multi_dynamic,
    &led_fx_mode_breath,
    &led_fx_mode_fade,
    &led_fx_mode_scan,
    &led_fx_mode_dual_scan,
    &led_fx_mode_theater_chase,
    &led_fx_mode_theater_chase_rainbow,
    &led_fx_mode_rainbow,
    &led_fx_mode_rainbow_cycle,
    &led_fx_mode_running_lights,
    &led_fx_mode_twinkle,
    &led_fx_mode_twinkle_random,
    &led_fx_mode_twinkle_fade,
    &led_fx_mode_twinkle_fade_random,
    &led_fx_mode_sparkle,
    &led_fx_mode_flash_sparkle,
    &led_fx_mode_hyper_sparkle,
    &led_fx_mode_multi_strobe,
};

static xQueueHandle led_effect_timer_queue;
static TaskHandle_t led_effect_task;
static led_effect_t* led_effects[MAX_EFFECTS];

_Noreturn static void led_fx_task(void *param) {
    while (true) {
        led_effect_timer_event_t evt;
        led_strip_t* strips[MAX_EFFECTS];
        xQueueReceive(led_effect_timer_queue, &evt, portMAX_DELAY);
        for (uint8_t i = 0; i < MAX_EFFECTS; i++) {
            if (led_effects[i] == NULL) {
                continue;
            }
            led_effect_t* led_effect = led_effects[i];
            if (!led_effect->running) {
                continue;
            }
            if (evt.timer_counter_value > led_effect->next_time) {
                uint64_t delay = led_effect->mode(led_effect);
                led_effect->next_time = evt.timer_counter_value + delay;
                led_effect->call++;
                for (uint8_t s = 0; s < MAX_EFFECTS; s++) {
                    if (strips[s] == led_effect->strip) {
                        break;
                    }
                    if (strips[s] != NULL) {
                        continue;
                    }
                    strips[s] = led_effect->strip;
                    break;
                }
            }
        }
        for (uint8_t s = 0; s < MAX_EFFECTS; s++) {
            if (strips[s] == NULL) {
                break;
            }
            led_strip_t* strip = strips[s];
            strip->refresh(strip, 100);
        }
    }
}

static bool IRAM_ATTR led_fx_timer_isr_callback(void *args) {
    BaseType_t high_task_awoken = pdFALSE;
    led_effect_timer_info_t *info = (led_effect_timer_info_t *)args;

    uint64_t timer_counter_value = timer_group_get_counter_value_in_isr(info->timer_group, info->timer_idx);

    led_effect_timer_event_t evt = {
        .info.timer_group = info->timer_group,
        .info.timer_idx = info->timer_idx,
        .info.alarm_interval = info->alarm_interval,
        .timer_counter_value = timer_counter_value
    };

    timer_counter_value += info->alarm_interval * TIMER_SCALE;
    timer_group_set_alarm_value_in_isr(info->timer_group, info->timer_idx, timer_counter_value);

    xQueueSendFromISR(led_effect_timer_queue, &evt, &high_task_awoken);

    return high_task_awoken == pdTRUE;
}

void led_fx_start(led_fx_t *led_fx) {
    ((led_effect_t *)led_fx)->running = true;
}

void led_fx_stop(led_fx_t *led_fx) {
    ((led_effect_t *)led_fx)->running = false;
}

void led_fx_set_first(led_fx_t *led_fx, uint32_t first) {
    led_effect_t *led_effect = (led_effect_t *)led_fx;
    led_effect->first = first;
    led_effect->len = led_effect->last - first + 1;
}

void led_fx_set_last(led_fx_t *led_fx, uint32_t last) {
    led_effect_t *led_effect = (led_effect_t *)led_fx;
    led_effect->last = last == 0 ? led_effect->strip->len - 1 : last;
    led_effect->len = last == 0 ? led_effect->strip->len : last - led_effect->first + 1;
}

void led_fx_set_color(led_fx_t *led_fx, led_color_t color, uint8_t index) {
    ((led_effect_t *)led_fx)->colors[index] = color;
}

void led_fx_set_mode(led_fx_t *led_fx, led_fx_mode_t mode) {
    ((led_effect_t *)led_fx)->mode = led_fx_modes[mode];
}

void led_fx_set_delay(led_fx_t *led_fx, uint32_t delay) {
    ((led_effect_t *)led_fx)->delay = delay;
}

void led_fx_set_reverse(led_fx_t *led_fx, bool reverse) {
    ((led_effect_t *)led_fx)->options.reverse = reverse;
}

void led_fx_set_size(led_fx_t *led_fx, led_fx_size_t size) {
    ((led_effect_t *)led_fx)->options.size = size;
}

void led_fx_set_fade(led_fx_t *led_fx, led_fx_fade_t fade) {
    ((led_effect_t *)led_fx)->options.fade = fade;
}

esp_err_t led_fx_setup(timer_group_t group, timer_idx_t timer, int timer_interval) {
    esp_err_t ret = ESP_OK;

    led_effect_timer_queue = xQueueCreate(10, sizeof(led_effect_timer_event_t));
    STRIP_FX_CHECK(led_effect_timer_queue != NULL, "create timer queue failed", err, ESP_FAIL)
    xTaskCreate(led_fx_task, "led_fx_task", 4096, NULL, tskIDLE_PRIORITY, &led_effect_task);
    STRIP_FX_CHECK(led_effect_task != NULL, "create task failed", queue, ESP_FAIL)

    timer_config_t config = {
        .divider = TIMER_DIVIDER,
        .counter_dir = TIMER_COUNT_UP,
        .counter_en = TIMER_PAUSE,
        .alarm_en = TIMER_ALARM_EN,
        .auto_reload = TIMER_AUTORELOAD_DIS,
    };
    STRIP_FX_CHECK(timer_init(group, timer, &config) == ESP_OK, "init timer failed", task, ESP_FAIL)

    STRIP_FX_CHECK(timer_set_counter_value(group, timer, 0) == ESP_OK, "set timer counter failed", task, ESP_FAIL)
    STRIP_FX_CHECK(timer_set_alarm_value(group, timer, timer_interval * TIMER_SCALE) == ESP_OK, "set timer alarm failed", task, ESP_FAIL)
    STRIP_FX_CHECK(timer_enable_intr(group, timer) == ESP_OK, "init timer interrupt failed", task, ESP_FAIL)

    led_effect_timer_info_t *timer_info = calloc(1, sizeof(led_effect_timer_info_t));
    timer_info->timer_group = group;
    timer_info->timer_idx = timer;
    timer_info->alarm_interval = timer_interval;
    STRIP_FX_CHECK(timer_isr_callback_add(group, timer, led_fx_timer_isr_callback, timer_info, 0) == ESP_OK,
            "add timer isr callback failed", task, ESP_FAIL)

    STRIP_FX_CHECK(timer_start(group, timer) == ESP_OK, "start timer failed", task, ESP_FAIL)

    return ESP_OK;
task:
    vTaskDelete(&led_effect_task);
queue:
    vQueueDelete(led_effect_timer_queue);
err:
    return ret;
}

led_fx_t *led_fx_init(led_strip_t *led_strip, led_fx_config_t *config) {
    led_fx_t *ret = NULL;
    STRIP_FX_CHECK(led_strip, "LED strip can't be null", err, NULL);

    uint32_t led_effect_size = sizeof(led_effect_t);
    led_effect_t *led_effect = calloc(1, led_effect_size);
    STRIP_FX_CHECK(led_effect, "request memory for LED strip effects failed", err, NULL);

    led_effect->strip = led_strip;
    led_effect->running = false;
    led_effect->first = config->first;
    led_effect->last = config->last == 0 ? led_strip->len - 1 : config->last;
    led_effect->len = config->last == 0 ? led_strip->len : config->last - config->first + 1;
    memcpy(led_effect->colors, config->colors, MAX_COLORS * sizeof(led_color_t));
    led_effect->mode = led_fx_modes[config->mode];
    led_effect->delay = config->delay == 0 ? DEFAULT_DELAY : config->delay;

    ESP_LOGD(TAG, "init effect from %u to %u with length %u",
             led_effect->first, led_effect->last, led_effect->len);
    if (LOG_LOCAL_LEVEL >= ESP_LOG_DEBUG) {
        for (uint8_t c = 0; c < MAX_COLORS; c++) {
            ESP_LOGD(TAG, "init effect color %u rgb(%u, %u, %u)",
                     c, led_effect->colors[c].r, led_effect->colors[c].g, led_effect->colors[c].b);
        }
    }

    for (uint8_t i = 0; i < MAX_EFFECTS; i++) {
        if (led_effects[i] == NULL) {
            led_effects[i] = led_effect;
            break;
        }
    }

    return (led_fx_t *)led_effect;
err:
    return ret;
}

