#include <esp_system.h>
#include <esp_log.h>
#include "led_fx_helpers.h"
#include "led_color.h"

static const char *TAG = "led_fx_helpers";

static const uint8_t sine_wave[256] = {
        0x80, 0x83, 0x86, 0x89, 0x8C, 0x90, 0x93, 0x96,
        0x99, 0x9C, 0x9F, 0xA2, 0xA5, 0xA8, 0xAB, 0xAE,
        0xB1, 0xB3, 0xB6, 0xB9, 0xBC, 0xBF, 0xC1, 0xC4,
        0xC7, 0xC9, 0xCC, 0xCE, 0xD1, 0xD3, 0xD5, 0xD8,
        0xDA, 0xDC, 0xDE, 0xE0, 0xE2, 0xE4, 0xE6, 0xE8,
        0xEA, 0xEB, 0xED, 0xEF, 0xF0, 0xF1, 0xF3, 0xF4,
        0xF5, 0xF6, 0xF8, 0xF9, 0xFA, 0xFA, 0xFB, 0xFC,
        0xFD, 0xFD, 0xFE, 0xFE, 0xFE, 0xFF, 0xFF, 0xFF,
        0xFF, 0xFF, 0xFF, 0xFF, 0xFE, 0xFE, 0xFE, 0xFD,
        0xFD, 0xFC, 0xFB, 0xFA, 0xFA, 0xF9, 0xF8, 0xF6,
        0xF5, 0xF4, 0xF3, 0xF1, 0xF0, 0xEF, 0xED, 0xEB,
        0xEA, 0xE8, 0xE6, 0xE4, 0xE2, 0xE0, 0xDE, 0xDC,
        0xDA, 0xD8, 0xD5, 0xD3, 0xD1, 0xCE, 0xCC, 0xC9,
        0xC7, 0xC4, 0xC1, 0xBF, 0xBC, 0xB9, 0xB6, 0xB3,
        0xB1, 0xAE, 0xAB, 0xA8, 0xA5, 0xA2, 0x9F, 0x9C,
        0x99, 0x96, 0x93, 0x90, 0x8C, 0x89, 0x86, 0x83,
        0x80, 0x7D, 0x7A, 0x77, 0x74, 0x70, 0x6D, 0x6A,
        0x67, 0x64, 0x61, 0x5E, 0x5B, 0x58, 0x55, 0x52,
        0x4F, 0x4D, 0x4A, 0x47, 0x44, 0x41, 0x3F, 0x3C,
        0x39, 0x37, 0x34, 0x32, 0x2F, 0x2D, 0x2B, 0x28,
        0x26, 0x24, 0x22, 0x20, 0x1E, 0x1C, 0x1A, 0x18,
        0x16, 0x15, 0x13, 0x11, 0x10, 0x0F, 0x0D, 0x0C,
        0x0B, 0x0A, 0x08, 0x07, 0x06, 0x06, 0x05, 0x04,
        0x03, 0x03, 0x02, 0x02, 0x02, 0x01, 0x01, 0x01,
        0x01, 0x01, 0x01, 0x01, 0x02, 0x02, 0x02, 0x03,
        0x03, 0x04, 0x05, 0x06, 0x06, 0x07, 0x08, 0x0A,
        0x0B, 0x0C, 0x0D, 0x0F, 0x10, 0x11, 0x13, 0x15,
        0x16, 0x18, 0x1A, 0x1C, 0x1E, 0x20, 0x22, 0x24,
        0x26, 0x28, 0x2B, 0x2D, 0x2F, 0x32, 0x34, 0x37,
        0x39, 0x3C, 0x3F, 0x41, 0x44, 0x47, 0x4A, 0x4D,
        0x4F, 0x52, 0x55, 0x58, 0x5B, 0x5E, 0x61, 0x64,
        0x67, 0x6A, 0x6D, 0x70, 0x74, 0x77, 0x7A, 0x7D
};

void led_fx_fill(led_strip_t *led_strip, led_color_t color, uint32_t first, uint32_t count) {
    uint32_t end;

    if (first >= led_strip->len) {
        ESP_LOGD(TAG, "not filling %u >= %u", first, led_strip->len);
        return;
    }

    if (count == 0) {
        end = led_strip->len;
    } else {
        end = first + count;
    }

    ESP_LOGD(TAG, "filling rgb(%u, %u, %u) from %u to %u", color.r, color.g, color.b, first, end);
    for (uint32_t i = first; i < end; i++) {
        led_strip->set_pixel(led_strip, i % led_strip->len, color);
    }
}

uint32_t led_fx_blink(led_effect_t *led_effect, led_color_t color1, led_color_t color2, bool strobe) {
    if(led_effect->call & 1) {
        led_color_t color = led_effect->options.reverse ? color1 : color2; // off
        led_fx_fill(led_effect->strip, color, led_effect->first, led_effect->len);
        return strobe ? led_effect->delay - 20 : (led_effect->delay / 2);
    } else {
        led_color_t color = led_effect->options.reverse ? color2 : color1; // on
        led_fx_fill(led_effect->strip, color, led_effect->first, led_effect->len);
        return strobe ? 20 : (led_effect->delay / 2);
    }
}

led_color_t led_fx_color_blend(led_color_t color1, led_color_t color2, uint8_t blend_amount) {
    led_color_t color;
    color.r = blend_amount * (color2.r - color1.r) / 256 + color1.r;
    color.g = blend_amount * (color2.g - color1.g) / 256 + color1.g;
    color.b = blend_amount * (color2.b - color1.b) / 256 + color1.b;

    ESP_LOGD(TAG, "blend rgb(%u, %u, %u) and rgb(%u, %u, %u) with %u to rgb(%u, %u, %u)",
             color1.r, color1.g, color1.b, color2.r, color2.g, color2.b, blend_amount, color.r, color.g, color.b);

    return color;
}

uint32_t led_fx_color_wipe(led_effect_t *led_effect, led_color_t color1, led_color_t color2, bool rev) {
    if (led_effect->step < led_effect->len) {
        uint32_t led_offset = led_effect->step;
        if (led_effect->options.reverse) {
            led_effect->strip->set_pixel(led_effect->strip, led_effect->last - led_offset, color1);
        } else {
            led_effect->strip->set_pixel(led_effect->strip, led_effect->first + led_offset, color1);
        }
    } else {
        uint32_t led_offset = led_effect->step - led_effect->len;
        if ((led_effect->options.reverse && !rev) || (!led_effect->options.reverse && rev)) {
            led_effect->strip->set_pixel(led_effect->strip, led_effect->last - led_offset, color2);
        } else {
            led_effect->strip->set_pixel(led_effect->strip, led_effect->first + led_offset, color2);
        }
    }

    led_effect->step = (led_effect->step + 1) % (led_effect->len * 2);

    return (led_effect->delay / (led_effect->len * 2));
}

uint32_t led_fx_random(uint32_t lower, uint32_t upper) {
    return (esp_random() % (upper - lower + 1)) + lower;
}

uint8_t led_fx_random8() {
    uint8_t r;
    esp_fill_random(&r, sizeof(uint8_t));
    return r;
}

uint8_t led_fx_get_random_wheel_index(uint8_t pos) {
    uint8_t r, x, y, d = 0;

    while (d < 42) {
        r = led_fx_random8();
        x = abs(pos - r);
        y = 255 - x;
        d = x < y ? x : y;
    }

    return r;
}

uint32_t led_fx_scan(led_effect_t *led_effect, bool dual) {
    int8_t dir = led_effect->aux_param ? -1 : 1;
    uint8_t size = 1 << led_effect->options.size;
    ESP_LOGD(TAG, "dir: %d, size: %u, step: %u", dir, size, led_effect->step);

    led_fx_fill(led_effect->strip, led_effect->colors[1], led_effect->first, led_effect->len);

    for (uint8_t i = 0; i < size; i++) {
        if (led_effect->options.reverse || dual) {
            led_effect->strip->set_pixel(led_effect->strip, led_effect->last - led_effect->step - i, led_effect->colors[0]);
        }
        if (!led_effect->options.reverse || dual) {
            led_effect->strip->set_pixel(led_effect->strip, led_effect->first + led_effect->step + i, led_effect->colors[0]);
        }
    }

    led_effect->step += dir;
    if (led_effect->step == 0) {
        led_effect->aux_param = 0;
    }
    if (led_effect->step >= led_effect->len - size) led_effect->aux_param = 1;

    return (led_effect->delay / (led_effect->len * 2));
}

uint32_t led_fx_tricolor_chase(led_effect_t *led_effect, led_color_t color1, led_color_t color2, led_color_t color3) {
    uint8_t size1 = 1 << led_effect->options.size;
    uint8_t size2 = size1 + size1;
    uint8_t size3 = size2 + size1;
    uint32_t index = led_effect->step % size3;
    for (uint32_t i = 0; i < led_effect->len; i++, index++) {
        index = index % size3;

        led_color_t color = color3;
        if (index < size1) color = color1;
        else if (index < size2) color = color2;

        if (led_effect->options.reverse) {
            led_effect->strip->set_pixel(led_effect->strip, led_effect->first + i, color);
        } else {
            led_effect->strip->set_pixel(led_effect->strip, led_effect->last - i, color);
        }
    }

    led_effect->step++;

    return (led_effect->delay / led_effect->len);
}

uint8_t led_fx_sine8(uint8_t index) {
    return sine_wave[index];
}

uint32_t led_fx_twinkle(led_effect_t *led_effect, led_color_t color1, led_color_t color2) {
    if (led_effect->step == 0) {
        led_fx_fill(led_effect->strip, color2, led_effect->first, led_effect->len);
        uint32_t min_leds = (led_effect->len / 4) + 1; // make sure, at least one LED is on
        led_effect->step = led_fx_random(min_leds, min_leds * 2);
    }

    led_effect->strip->set_pixel(led_effect->strip, led_effect->first + led_fx_random(0, led_effect->len - 1), color1);

    led_effect->step--;
    return (led_effect->delay / led_effect->len);
}

void led_fx_fade_out(led_effect_t *led_effect, led_color_t target_color) {
    static const uint8_t rateMapH[] = {0, 1, 1, 1, 2, 3, 4, 6};
    static const uint8_t rateMapL[] = {0, 2, 3, 8, 8, 8, 8, 8};

    uint8_t rate  = led_effect->options.fade;
    uint8_t rateH = rateMapH[rate];
    uint8_t rateL = rateMapL[rate];

    for (uint32_t i = led_effect->first; i <= led_effect->last; i++) {
        led_color_t color = led_effect->strip->get_pixel(led_effect->strip, i);
        if (rate == 0) { // old fade-to-black algorithm
            led_effect->strip->set_pixel(led_effect->strip, i, led_color_and(led_color_shift(color, 1), 0x7F));
        } else { // new fade-to-color algorithm
            led_color_t delta = led_color_difference(color, target_color);
            led_color_t faded_color = (led_color_t){
                .r = delta.r < 3 ? delta.r : (delta.r >> rateH) + (delta.r >> rateL),
                .g = delta.g < 3 ? delta.g : (delta.g >> rateH) + (delta.g >> rateL),
                .b = delta.b < 3 ? delta.b : (delta.b >> rateH) + (delta.b >> rateL),
                .a = delta.a < 3 ? delta.a : (delta.a >> rateH) + (delta.a >> rateL),
            };
            led_effect->strip->set_pixel(led_effect->strip, i, led_color_sum(color, faded_color));
        }
    }
}

uint32_t led_fx_twinkle_fade(led_effect_t *led_effect, led_color_t color) {
    led_fx_fade_out(led_effect, led_effect->colors[1]);

    if (led_fx_random(0, 3) == 0) {
        uint8_t size = 1 << led_effect->options.size;
        uint16_t index = led_effect->first + led_fx_random(0, led_effect->len - size + 1);
        led_fx_fill(led_effect->strip, color, index, size);
    }

    return (led_effect->delay / 16);
}

uint32_t led_fx_sparkle(led_effect_t *led_effect, led_color_t color1, led_color_t color2) {
    if (led_effect->step == 0) {
        led_fx_fill(led_effect->strip, color1, led_effect->first, led_effect->len);
    }

    uint8_t size = 1 << led_effect->options.size;
    led_fx_fill(led_effect->strip, color1, led_effect->first + led_effect->aux_param3, size);

    led_effect->aux_param3 = led_fx_random(0, led_effect->len - size + 1);
    led_fx_fill(led_effect->strip, color2, led_effect->first + led_effect->aux_param3, size);

    return (led_effect->delay / 32);
}