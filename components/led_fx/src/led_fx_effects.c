#include <sys/param.h>
#include "led_fx_effects.h"
#include "led_color.h"

uint32_t led_fx_mode_static(led_effect_t *led_effect) {
    led_fx_fill(led_effect->strip, led_effect->colors[0], 0, 0);
    return led_effect->delay;
}

uint32_t led_fx_mode_blink(led_effect_t *led_effect) {
    return led_fx_blink(led_effect, led_effect->colors[0], led_effect->colors[1], false);
}

uint32_t led_fx_mode_blink_rainbow(led_effect_t *led_effect) {
    return led_fx_blink(led_effect, led_fx_color_wheel(led_effect->call & 0xFF), led_effect->colors[1], false);
}

uint32_t led_fx_mode_strobe(led_effect_t *led_effect) {
    return led_fx_blink(led_effect, led_effect->colors[0], led_effect->colors[1], true);
}

uint32_t led_fx_mode_strobe_rainbow(led_effect_t *led_effect) {
    return led_fx_blink(led_effect, led_fx_color_wheel(led_effect->call & 0xFF), led_effect->colors[1], true);
}

uint32_t led_fx_mode_breath(led_effect_t *led_effect) {
    uint32_t lum = led_effect->step;
    if (lum > 255) lum = 511 - lum; // lum = 15 -> 255 -> 15

    uint32_t delay;
    if(lum == 15) delay = 970; // 970 pause before each breath
    else if(lum <=  25) delay = 38; // 19
    else if(lum <=  50) delay = 36; // 18
    else if(lum <=  75) delay = 28; // 14
    else if(lum <= 100) delay = 20; // 10
    else if(lum <= 125) delay = 14; // 7
    else if(lum <= 150) delay = 11; // 5
    else delay = 10; // 4

    led_color_t color = led_fx_color_blend(led_effect->colors[1], led_effect->colors[0], lum);
    led_fx_fill(led_effect->strip, color, led_effect->first, led_effect->len);

    led_effect->step += 2;
    if (led_effect->step > (512-15)) {
        led_effect->step = 15;
    }
    return delay;
}

uint32_t led_fx_mode_color_wipe(led_effect_t * led_effect) {
    return led_fx_color_wipe(led_effect, led_effect->colors[0], led_effect->colors[1], false);
}

uint32_t led_fx_mode_color_wipe_inv(led_effect_t * led_effect) {
    return led_fx_color_wipe(led_effect, led_effect->colors[1], led_effect->colors[0], false);
}

uint32_t led_fx_mode_color_wipe_rev(led_effect_t * led_effect) {
    return led_fx_color_wipe(led_effect, led_effect->colors[0], led_effect->colors[1], true);
}

uint32_t led_fx_mode_color_wipe_rev_inv(led_effect_t * led_effect) {
    return led_fx_color_wipe(led_effect, led_effect->colors[1], led_effect->colors[0], true);
}

uint32_t led_fx_mode_color_wipe_random(led_effect_t *led_effect) {
    if (led_effect->step % led_effect->len == 0) { // aux_param will store our random color wheel index
        led_effect->aux_param = led_fx_get_random_wheel_index(led_effect->aux_param);
    }
    led_color_t color = led_fx_color_wheel(led_effect->aux_param);
    return led_fx_color_wipe(led_effect, color, color, false) * 2;
}

uint32_t led_fx_mode_random_color(led_effect_t *led_effect) {
    led_effect->aux_param = led_fx_get_random_wheel_index(led_effect->aux_param); // aux_param will store our random color wheel index
    led_color_t color =led_fx_color_wheel(led_effect->aux_param);
    led_fx_fill(led_effect->strip, color, led_effect->first, led_effect->len);
    return led_effect->delay;
}

uint32_t led_fx_mode_single_dynamic(led_effect_t *led_effect) {
    if (led_effect->call == 0) {
        for (uint32_t i = led_effect->first; i <= led_effect->last; i++) {
            led_effect->strip->set_pixel(led_effect->strip, i, led_fx_color_wheel(led_fx_random8()));
        }
    }

    led_effect->strip->set_pixel(led_effect->strip, led_effect->first + led_fx_random(0, led_effect->len), led_fx_color_wheel(led_fx_random8()));
    return led_effect->delay;
}

uint32_t led_fx_mode_multi_dynamic(led_effect_t *led_effect) {
    for(uint32_t i = led_effect->first; i <= led_effect->last; i++) {
        led_effect->strip->set_pixel(led_effect->strip, i, led_fx_color_wheel(led_fx_random8()));
    }

    return led_effect->delay;
}

uint32_t led_fx_mode_rainbow(led_effect_t *led_effect) {
    led_color_t color = led_fx_color_wheel(led_effect->step);
    led_fx_fill(led_effect->strip, color, led_effect->first, led_effect->len);

    led_effect->step = (led_effect->step + 1) & 0xFF;
    return (led_effect->delay / 256);
}

uint32_t led_fx_mode_color_sweep_random(led_effect_t *led_effect) {
    if(led_effect->step % led_effect->len == 0) {
        led_effect->aux_param = led_fx_get_random_wheel_index(led_effect->aux_param);
    }
    led_color_t color = led_fx_color_wheel(led_effect->aux_param);
    return led_fx_color_wipe(led_effect, color, color, true) * 2;
}

uint32_t led_fx_mode_rainbow_cycle(led_effect_t *led_effect) {
    for (uint32_t i = 0; i < led_effect->len; i++) {
        led_color_t color = led_fx_color_wheel(((i * 256 / led_effect->len) + led_effect->step) & 0xFF);
        if (led_effect->options.reverse) {
            led_effect->strip->set_pixel(led_effect->strip, led_effect->first - i, color);
        } else {
            led_effect->strip->set_pixel(led_effect->strip, led_effect->first + i, color);
        }
    }

    led_effect->step = (led_effect->step + 1) & 0xFF;
    return (led_effect->delay / 256);
}

uint32_t led_fx_mode_scan(led_effect_t *led_effect) {
    return led_fx_scan(led_effect, false);
}

uint32_t led_fx_mode_dual_scan(led_effect_t *led_effect) {
    return led_fx_scan(led_effect, true);
}

uint32_t led_fx_mode_fade(led_effect_t *led_effect) {
    uint32_t lum = led_effect->step;
    if(lum > 255) lum = 511 - lum; // lum = 0 -> 255 -> 0

    led_color_t color = led_fx_color_blend(led_effect->colors[1], led_effect->colors[0], lum);
    led_fx_fill(led_effect->strip, color, led_effect->first, led_effect->len);

    led_effect->step += 4;
    if (led_effect->step > 511) {
        led_effect->step = 0;
    }
    return (led_effect->delay / 128);
}

uint32_t led_fx_mode_theater_chase(led_effect_t *led_effect) {
    return led_fx_tricolor_chase(led_effect, led_effect->colors[0], led_effect->colors[1], led_effect->colors[1]);
}

uint32_t led_fx_mode_theater_chase_rainbow(led_effect_t *led_effect) {
    led_effect->aux_param = (led_effect->aux_param + 1) & 0xFF;
    led_color_t color = led_fx_color_wheel(led_effect->aux_param);
    return led_fx_tricolor_chase(led_effect, color, led_effect->colors[1], led_effect->colors[1]);
}

uint32_t led_fx_mode_running_lights(led_effect_t *led_effect) {
    uint8_t size = 1 << led_effect->options.size;
    uint8_t sine_incr = MAX(1, (256 / led_effect->len) * size);
    for (uint32_t i = 0; i < led_effect->len; i++) {
        uint8_t lum = led_fx_sine8(((i + led_effect->step) * sine_incr));
        led_color_t color = led_fx_color_blend(led_effect->colors[0], led_effect->colors[1], lum);
        if(led_effect->options.reverse) {
            led_effect->strip->set_pixel(led_effect->strip, led_effect->first + i, color);
        } else {
            led_effect->strip->set_pixel(led_effect->strip, led_effect->last - i,  color);
        }
    }

    led_effect->step = (led_effect->step + 1) % 0xFF;

    return (led_effect->delay / led_effect->len);
}

uint32_t led_fx_mode_twinkle(led_effect_t *led_effect) {
    return led_fx_twinkle(led_effect, led_effect->colors[0], led_effect->colors[1]);
}

uint32_t led_fx_mode_twinkle_random(led_effect_t *led_effect) {
    return led_fx_twinkle(led_effect, led_fx_color_wheel(led_fx_random8()), led_effect->colors[1]);
}

uint32_t led_fx_mode_twinkle_fade(led_effect_t *led_effect) {
    return led_fx_twinkle_fade(led_effect, led_effect->colors[0]);
}

uint32_t led_fx_mode_twinkle_fade_random(led_effect_t *led_effect) {
    return led_fx_twinkle_fade(led_effect, led_fx_color_wheel(led_fx_random8()));
}

uint32_t led_fx_mode_sparkle(led_effect_t *led_effect) {
    return led_fx_sparkle(led_effect, led_effect->colors[1], led_effect->colors[0]);
}

uint32_t led_fx_mode_flash_sparkle(led_effect_t *led_effect) {
    return led_fx_sparkle(led_effect, led_effect->colors[0], WHITE);
}

uint32_t led_fx_mode_hyper_sparkle(led_effect_t *led_effect) {
    led_fx_fill(led_effect->strip, led_effect->colors[0], led_effect->first, led_effect->len);

    uint8_t size = 1 << led_effect->options.size;
    for (uint8_t i = 0; i < 8; i++) {
        led_fx_fill(led_effect->strip, WHITE, led_effect->first + led_fx_random(0, led_effect->len - size + 1), size);
    }

    return (led_effect->delay / 32);
}

uint32_t led_fx_mode_multi_strobe(led_effect_t *led_effect) {
    led_fx_fill(led_effect->strip, led_effect->colors[1], led_effect->first, led_effect->len);

    uint16_t delay = 200 + ((9 - (led_effect->delay % 10)) * 100);
    uint16_t count = 2 * ((led_effect->delay / 100) + 1);
    if (led_effect->step < count) {
        if ((led_effect->step & 1) == 0) {
            led_fx_fill(led_effect->strip, led_effect->colors[0], led_effect->first, led_effect->len);
            delay = 20;
        } else {
            delay = 50;
        }
    }

    led_effect->step = (led_effect->step + 1) % (count + 1);
    return delay;
}