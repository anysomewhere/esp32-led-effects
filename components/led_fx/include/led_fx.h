#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <driver/timer.h>
#include "led_strip.h"

#define MAX_EFFECTS 8
#define MAX_COLORS 8
#define DEFAULT_DELAY 1000

typedef void *led_fx_t;

typedef enum {
    LED_FX_STATIC,
    LED_FX_BLINK,
    LED_FX_BLINK_RAINBOW,
    LED_FX_COLOR_WIPE,
    LED_FX_COLOR_WIPE_INV,
    LED_FX_COLOR_WIPE_REV,
    LED_FX_COLOR_WIPE_REV_INV,
    LED_FX_COLOR_WIPE_RANDOM,
    LED_FX_COLOR_SWEEP_RANDOM,
    LED_FX_RANDOM_COLOR,
    LED_FX_SINGLE_DYNAMIC,
    LED_FX_MULTI_DYNAMIC,
    LED_FX_BREATH,
    LED_FX_FADE,
    LED_FX_SCAN,
    LED_FX_DUAL_SCAN,
    LED_FX_THEATER_CHASE,
    LED_FX_THEATER_CHASE_RAINBOW,
    LED_FX_RAINBOW,
    LED_FX_RAINBOX_CYCLE,
    LED_FX_RUNNING_LIGHTS,
    LED_FX_TWINKLE,
    LED_FX_TWINKLE_RANDOM,
    LED_FX_TWINKLE_FADE,
    LED_FX_TWINKLE_FADE_RANDOM,
    LED_FX_SPARKLE,
    LED_FX_FLASH_SPARKLE,
    LED_FX_HYPER_SPARKLE,
    LED_FX_MULTI_STROBE,
} led_fx_mode_t;

typedef enum {
    LED_FX_SIZE_S,
    LED_FX_SIZE_M,
    LED_FX_SIZE_L,
    LED_FX_SIZE_XL,
} led_fx_size_t;

typedef enum {
    LED_FX_FADE_XFAST,
    LED_FX_FADE_FAST,
    LED_FX_FADE_MEDIUM,
    LED_FX_FADE_SLOW,
    LED_FX_FADE_XSLOW,
    LED_FX_FADE_XXSLOW,
    LED_FX_FADE_GLACIAL,
} led_fx_fade_t;

typedef struct {
    uint32_t first;
    uint32_t last;
    led_color_t colors[MAX_COLORS];
    led_fx_mode_t mode;
    uint32_t delay;
} led_fx_config_t;

esp_err_t led_fx_setup(timer_group_t group, timer_idx_t timer, int timer_interval);
led_fx_t *led_fx_init(led_strip_t *led_strip, led_fx_config_t *config);
void led_fx_start(led_fx_t *led_fx);
void led_fx_stop(led_fx_t *led_fx);
void led_fx_set_first(led_fx_t *led_fx, uint32_t first);
void led_fx_set_last(led_fx_t *led_fx, uint32_t last);
void led_fx_set_color(led_fx_t *led_fx, led_color_t color, uint8_t index);
void led_fx_set_mode(led_fx_t *led_fx, led_fx_mode_t mode);
void led_fx_set_delay(led_fx_t *led_fx, uint32_t delay);
void led_fx_set_reverse(led_fx_t *led_fx, bool reverse);
void led_fx_set_size(led_fx_t *led_fx, led_fx_size_t size);
void led_fx_set_fade(led_fx_t *led_fx, led_fx_fade_t fade);

#ifdef __cplusplus
}
#endif