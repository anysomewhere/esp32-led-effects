#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "led_fx_helpers.h"

uint32_t led_fx_mode_static(led_effect_t *led_effect);
uint32_t led_fx_mode_blink(led_effect_t *led_effect);
uint32_t led_fx_mode_blink_rainbow(led_effect_t *led_effect);
uint32_t led_fx_mode_strobe(led_effect_t *led_effect);
uint32_t led_fx_mode_strobe_rainbow(led_effect_t *led_effect);
uint32_t led_fx_mode_breath(led_effect_t *led_effect);
uint32_t led_fx_mode_color_wipe(led_effect_t * led_effect);
uint32_t led_fx_mode_color_wipe_inv(led_effect_t * led_effect);
uint32_t led_fx_mode_color_wipe_rev(led_effect_t * led_effect);
uint32_t led_fx_mode_color_wipe_rev_inv(led_effect_t * led_effect);
uint32_t led_fx_mode_color_wipe_random(led_effect_t *led_effect);
uint32_t led_fx_mode_color_sweep_random(led_effect_t *led_effect);
uint32_t led_fx_mode_random_color(led_effect_t *led_effect);
uint32_t led_fx_mode_single_dynamic(led_effect_t *led_effect);
uint32_t led_fx_mode_multi_dynamic(led_effect_t *led_effect);
uint32_t led_fx_mode_rainbow(led_effect_t *led_effect);
uint32_t led_fx_mode_rainbow_cycle(led_effect_t *led_effect);
uint32_t led_fx_mode_scan(led_effect_t *led_effect);
uint32_t led_fx_mode_dual_scan(led_effect_t *led_effect);
uint32_t led_fx_mode_fade(led_effect_t *led_effect);
uint32_t led_fx_mode_theater_chase(led_effect_t *led_effect);
uint32_t led_fx_mode_theater_chase_rainbow(led_effect_t *led_effect);
uint32_t led_fx_mode_running_lights(led_effect_t *led_effect);
uint32_t led_fx_mode_twinkle(led_effect_t *led_effect);
uint32_t led_fx_mode_twinkle_random(led_effect_t *led_effect);
uint32_t led_fx_mode_twinkle_fade(led_effect_t *led_effect);
uint32_t led_fx_mode_twinkle_fade_random(led_effect_t *led_effect);
uint32_t led_fx_mode_sparkle(led_effect_t *led_effect);
uint32_t led_fx_mode_flash_sparkle(led_effect_t *led_effect);
uint32_t led_fx_mode_hyper_sparkle(led_effect_t *led_effect);
uint32_t led_fx_mode_multi_strobe(led_effect_t *led_effect);

#ifdef __cplusplus
}
#endif
