#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "led_strip.h"
#include "led_fx.h"

typedef struct led_effect_s led_effect_t;

typedef uint32_t (*led_fx_mode)(led_effect_t *led_effect);

typedef struct {
    bool reverse: 1;
    led_fx_size_t size: 2;
    led_fx_fade_t fade: 3;
} led_effect_options_t;

struct led_effect_s {
    led_strip_t *strip;
    bool running;
    led_color_t colors[MAX_COLORS];
    led_effect_options_t options;
    uint8_t aux_param;
    uint8_t aux_param2;
    uint32_t aux_param3;
    uint32_t first;
    uint32_t last;
    uint32_t len;
    uint64_t next_time;
    uint32_t delay;
    uint32_t step;
    uint32_t call;
    led_fx_mode mode;
};

void led_fx_fill(led_strip_t *led_strip, led_color_t color, uint32_t first, uint32_t count);
uint32_t led_fx_blink(led_effect_t *led_effect, led_color_t color1, led_color_t color2, bool strobe);
led_color_t led_fx_color_blend(led_color_t color1, led_color_t color2, uint8_t blend_amount);
uint32_t led_fx_color_wipe(led_effect_t *led_effect, led_color_t color1, led_color_t color2, bool rev);
uint8_t led_fx_get_random_wheel_index(uint8_t pos);
uint32_t led_fx_random(uint32_t lower, uint32_t upper);
uint8_t led_fx_random8();
uint32_t led_fx_scan(led_effect_t *led_effect, bool dual);
uint32_t led_fx_tricolor_chase(led_effect_t *led_effect, led_color_t color1, led_color_t color2, led_color_t color3);
uint8_t led_fx_sine8(uint8_t index);
uint32_t led_fx_twinkle(led_effect_t *led_effect, led_color_t color1, led_color_t color2);
uint32_t led_fx_twinkle_fade(led_effect_t *led_effect, led_color_t color);
uint32_t led_fx_sparkle(led_effect_t *led_effect, led_color_t color1, led_color_t color2);

#ifdef __cplusplus
}
#endif
