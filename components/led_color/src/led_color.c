#include <stdlib.h>
#include "led_color.h"

led_color_t led_fx_color_wheel(uint8_t pos) {
    pos = 255 - pos;
    if(pos < 85) {
        return (led_color_t){
            .r = 255 - pos * 3,
            .g = 0,
            .b = pos * 3,
        };
    } else if(pos < 170) {
        pos -= 85;
        return (led_color_t){
            .r = 0,
            .g = pos * 3,
            .b = 255 - pos * 3,
        };
    } else {
        pos -= 170;
        return (led_color_t){
            .r = pos * 3,
            .g = 255 - pos * 3,
            .b = 0,
        };
    }
}

led_color_t led_color_difference(led_color_t color1, led_color_t color2) {
    return (led_color_t){
          .r = abs(color2.r - color1.r),
          .g = abs(color2.g - color1.g),
          .b = abs(color2.b - color1.b),
          .a = abs(color2.a - color1.a),
    };
}

led_color_t led_color_sum(led_color_t color1, led_color_t color2) {
    return (led_color_t){
          .r = color2.r + color1.r,
          .g = color2.g + color1.g,
          .b = color2.b + color1.b,
          .a = color2.a + color1.a,
    };
}

led_color_t led_color_shift(led_color_t color, int8_t shift) {
    if (shift < 0) {
        return (led_color_t){
            .r = color.r << -shift,
            .g = color.g << -shift,
            .b = color.b << -shift,
            .a = color.a << -shift,
        };
    } else if (shift > 0) {
        return (led_color_t){
            .r = color.r >> shift,
            .g = color.g >> shift,
            .b = color.b >> shift,
            .a = color.a >> shift,
        };
    } else {
        return color;
    }
}

led_color_t led_color_and(led_color_t color, uint8_t mask) {
    return (led_color_t){
            .r = color.r & mask,
            .g = color.g & mask,
            .b = color.b & mask,
            .a = color.a & mask,
    };
}