#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#define LED_COLOR(rc, gc, bc) (const led_color_t){ .b = (bc), .g = (gc), .r = (rc), .a = 0x00 }

#define RED        LED_COLOR(0xFF, 0x00, 0x00)
#define GREEN      (led_color_t)0x00FF00
#define BLUE       LED_COLOR(0x00, 0x00, 0xFF)
#define WHITE      LED_COLOR(0xFF, 0xFF, 0xFF)
#define BLACK      LED_COLOR(0x00, 0x00, 0x00)
#define YELLOW     LED_COLOR(0xFF, 0xFF, 0x00)
#define CYAN       (led_color_t)0x00FFFF
#define MAGENTA    (led_color_t)0xFF00FF
#define PURPLE     (led_color_t)0x400080
#define ORANGE     LED_COLOR(0xFF, 0x30, 0x00)
#define PINK       (led_color_t)0xFF1493
#define GRAY       (led_color_t)0x101010

/**
 * @brief Declare of LED Color Type
 */
typedef struct {
    uint8_t b: 8;
    uint8_t g: 8;
    uint8_t r: 8;
    uint8_t a: 8;
} led_color_t;

led_color_t led_fx_color_wheel(uint8_t pos);
led_color_t led_color_difference(led_color_t color1, led_color_t color2);
led_color_t led_color_sum(led_color_t color1, led_color_t color2);
led_color_t led_color_shift(led_color_t color, int8_t shift);
led_color_t led_color_and(led_color_t color, uint8_t mask);

#ifdef __cplusplus
}
#endif