#include <esp_spi_flash.h>
#include "sdkconfig.h"
#include "esp_log.h"
#include "driver/rmt.h"
#include "led_strip.h"
#include "led_fx.h"

static const char *TAG = "example";

#define RMT_TX_CHANNEL RMT_CHANNEL_0

void app_main(void)
{
    rmt_config_t config = RMT_DEFAULT_CONFIG_TX(CONFIG_EXAMPLE_RMT_TX_GPIO, RMT_TX_CHANNEL);
    // set counter clock to 40MHz
    config.clk_div = 2;

    ESP_ERROR_CHECK(rmt_config(&config));
    ESP_ERROR_CHECK(rmt_driver_install(config.channel, 0, 0));

    ESP_ERROR_CHECK(led_fx_setup(TIMER_GROUP_0, TIMER_0, 1));

    led_strip_config_t strip_config = LED_STRIP_DEFAULT_CONFIG(CONFIG_EXAMPLE_STRIP_LED_NUMBER, (led_strip_dev_t)config.channel);
    led_strip_t *strip = led_strip_new_rmt_ws2812(&strip_config);
    if (!strip) {
        ESP_LOGE(TAG, "install WS2812 driver failed");
    }

    led_fx_config_t led1_config = {
        .first = 0,
        .last = 0,
        .colors = { RED, YELLOW },
        .mode = LED_FX_MULTI_STROBE,
    };
    led_fx_t *effect1 = led_fx_init(strip, &led1_config);
/*    led_fx_config_t led2_config = {
        .first = 6,
        .last = 12,
        .colors = { ORANGE, BLUE },
        .mode = LED_FX_COLOR_WIPE,
    };
    led_fx_t *effect2 = led_fx_init(strip, &led2_config);*/
    if (!effect1/* || !effect2*/) {
        ESP_LOGE(TAG, "init effect failed");
    }
    led_fx_start(effect1);
    // led_fx_start(effect2);
}
